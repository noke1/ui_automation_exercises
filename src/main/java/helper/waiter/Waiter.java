package helper.waiter;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
    
    private static WebDriverWait wait;
    private static WebDriver driver;
    private static final int TIMEOUT = 20;
    private static final int POLLING = 1000;

    public Waiter(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,TIMEOUT, POLLING);
    }


    public static void waitForElementToAppear(WebElement element) {
        wait
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
    }


    public static void waitForElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
    }

    public static void waitForAlertToAppear() {
        wait
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent());
    }

    /**
     * Checks if element is NOT visible on the page.
     * @param element
     * @return true if exceptions happen, so we know that element is not visible for sure
     */
    public static boolean isElementNotVisible(WebElement element) {
        try {
            return wait
                    .ignoring(StaleElementReferenceException.class)
                    .until(ExpectedConditions.not(ExpectedConditions.invisibilityOf(element)));
        } catch (NoSuchElementException | TimeoutException e) {
            return true;
        }
    }
}
