package helper.browser;

import org.openqa.selenium.WebDriver;

public interface Terminate {
    void terminate(WebDriver driver);
}
