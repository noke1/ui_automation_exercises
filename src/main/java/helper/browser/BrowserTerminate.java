package helper.browser;

import org.openqa.selenium.WebDriver;

public class BrowserTerminate implements Terminate {

    @Override
    public void terminate(WebDriver driver) {
        driver.close();
        driver.quit();
    }
}
