package helper.browser;

import org.openqa.selenium.WebDriver;

public interface Adjust {

    String APP_BASE_URL = "http://www.uitestingplayground.com/";

    void adjust(WebDriver driver);
}
