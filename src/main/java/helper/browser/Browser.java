package helper.browser;

import org.openqa.selenium.WebDriver;

public class Browser {

    public Initialize initialize;
    public Terminate terminate;
    public Adjust adjust;
    public String name;

    public Browser(Initialize initialize, Terminate terminate, Adjust adjust, String name) {
        this.initialize = initialize;
        this.terminate = terminate;
        this.adjust = adjust;
        this.name = name;
    }

    public WebDriver initialize() {
        return this.initialize.initialize();
    }

    public void terminate(WebDriver driver) {
        this.terminate.terminate(driver);
    }

    public void adjust(WebDriver driver) {
        this.adjust.adjust(driver);
    }
}
