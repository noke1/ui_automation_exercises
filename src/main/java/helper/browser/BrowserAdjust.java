package helper.browser;

import org.openqa.selenium.WebDriver;

public class BrowserAdjust implements Adjust {

    @Override
    public void adjust(WebDriver driver) {
        prepareBrowserSettings(driver);
        goToBaseUrl(driver);
    }

    private void prepareBrowserSettings(WebDriver driver) {
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
    }

    private void goToBaseUrl(WebDriver driver) {
        driver.get(APP_BASE_URL);
    }


}
