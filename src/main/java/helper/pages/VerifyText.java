package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class VerifyText extends CorePage{
    public VerifyText(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//span[normalize-space(text())='Welcome']")
    private WebElement welcomeText;

    public String getWelcomeText() {
        Waiter.waitForElementToAppear(welcomeText);
        return welcomeText.getText();
    }

}
