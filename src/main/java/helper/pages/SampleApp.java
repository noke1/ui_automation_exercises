package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SampleApp extends CorePage {

    public SampleApp(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@name='UserName']")
    private WebElement inputUsername;

    @FindBy(css = "input[name='Password']")
    private WebElement inputPassword;

    @FindBy(id = "login")
    private WebElement buttonLogin;

    @FindBy(id = "loginstatus")
    private WebElement labelUserWelcome;

    public void loginWith(String username, String password) {
        Waiter.waitForElementToAppear(inputUsername);
        inputUsername.sendKeys(username);
        Waiter.waitForElementToAppear(inputPassword);
        inputPassword.sendKeys(password);
        Waiter.waitForElementToAppear(buttonLogin);
        buttonLogin.click();
    }

    public String getMessageAfterLogIn() {
        Waiter.waitForElementToAppear(labelUserWelcome);
        return labelUserWelcome.getText();
    }


}
