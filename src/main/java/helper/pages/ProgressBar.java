package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProgressBar extends CorePage {

    public ProgressBar(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "startButton")
    private WebElement buttonStart;

    @FindBy(id = "stopButton")
    private WebElement buttonStop;

    @FindBy(id = "progressBar")
    private WebElement progressBar;

    @FindBy(id = "result")
    private WebElement labelResult;

    public ProgressBar clickButtonStart() {
        Waiter.waitForElementToAppear(buttonStart);
        buttonStart.click();
        return this;
    }

    public ProgressBar clickButtonStop() {
        Waiter.waitForElementToAppear(buttonStop);
        buttonStop.click();
        return this;
    }

    public ProgressBar stopProgressIfValueOf(String value) {
        Waiter.waitForElementToAppear(progressBar);
        String progress = "";
        while (!progress.equals(value)) {
            progress = progressBar.getText();
            //i need to think about what happens when progress is not moving forward - infinite loop? :)
        }
        clickButtonStop();
        return this;
    }

    public int getResultValue() {
        Waiter.waitForElementToAppear(labelResult);
        //"Result: 0, duration: 18931"
        final String[] resultValues = labelResult
                .getText()
                .replaceAll("[^-?0-9]+", " ")
                .trim()
                .split(" ");
        return Integer.parseInt(resultValues[0]);
    }




}
