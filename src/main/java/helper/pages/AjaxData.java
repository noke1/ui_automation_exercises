package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AjaxData extends LoadDelays {

    public AjaxData(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button#ajaxButton")
    private WebElement buttonTrigger;

    @FindBy(css = "div#content p")
    private WebElement labelDataLoaded;


    public AjaxData clickButtonTrigger() {
        Waiter.waitForElementToBeClickable(buttonTrigger);
        buttonTrigger.click();
        return this;
    }

    public String getLabelDataLoadedText() {
        Waiter.waitForElementToAppear(labelDataLoaded);
        return labelDataLoaded.getText();
    }

}
