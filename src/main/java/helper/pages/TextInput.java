package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TextInput extends CorePage {

    public TextInput(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "newButtonName")
    private WebElement inputMyButtonText;

    @FindBy(id = "updatingButton")
    private WebElement buttonButtonName;

    public TextInput insertTextOf(String text) {
        Waiter.waitForElementToAppear(inputMyButtonText);
        inputMyButtonText.sendKeys(text);
        return this;
    }

    public TextInput clickButtonCreateNewButton() {
        Waiter.waitForElementToAppear(buttonButtonName);
        buttonButtonName.click();
        return this;
    }

    public String getCreateNewButtonText() {
        Waiter.waitForElementToAppear(buttonButtonName);
        return buttonButtonName.getText();
    }


}
