package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ScrollBars extends CorePage{
    public ScrollBars(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "hidingButton")
    private WebElement buttonHidingButton;

    public ScrollBars clickButtonHiding() {
        Waiter.waitForElementToAppear(buttonHidingButton);
        moveToElement(buttonHidingButton);
        buttonHidingButton.click();
        return this;
    }

    public ScrollBars moveToElement(WebElement element) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(element).build().perform();
        return this;
    }

}
