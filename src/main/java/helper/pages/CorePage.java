package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CorePage {

    private final WebDriver driver;
    private final Waiter waiter;

    public CorePage(WebDriver driver) {
        this.driver = driver;
        this.waiter = new Waiter(getDriver());
        PageFactory.initElements(driver,this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public Waiter getWaiter() {
        return waiter;
    }
}
