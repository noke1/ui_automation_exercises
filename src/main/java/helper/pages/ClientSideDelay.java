package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ClientSideDelay extends CorePage{

    public ClientSideDelay(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button#ajaxButton")
    private WebElement buttonTriggerLogic;

    @FindBy(css = "#content p")
    private WebElement labelClientSideDelay;


    public ClientSideDelay clickButtonTriggerLogic() {
        Waiter.waitForElementToAppear(buttonTriggerLogic);
        buttonTriggerLogic.click();
        return this;
    }

    public String getLabelClientSideDelayText() {
        Waiter.waitForElementToAppear(labelClientSideDelay);
        return labelClientSideDelay.getText();
    }
}
