package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class MouseOver extends CorePage {

    public MouseOver(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "a.text-primary")
    private WebElement anchorClickMe;

    @FindBy(css = "a.text-warning")
    private WebElement anchorClickMeAfterHover;

    @FindBy(id = "clickCount")
    private WebElement spanClickCount;


    /**
     * Clicking anchor is not trivial in this exercise. After hovering mouse on anchor, anchor classes changes.
     * So: locate anchor, move to anchor (hover), locate anchor after hover (because classes changed), click anchor after
     * hover, move mouse to any other element (click count in this case) so I can click anchor again after.
     * @return
     */
    public MouseOver hoverAndClickClickMe() {
        final Actions actions = new Actions(getDriver());

        Waiter.waitForElementToAppear(anchorClickMe);
        actions.moveToElement(anchorClickMe).build().perform();
        Waiter.waitForElementToAppear(anchorClickMeAfterHover);
        anchorClickMeAfterHover.click();
        actions.moveToElement(spanClickCount).build().perform(); //this is just to lose focus so i can click anchor again after

        return this;
    }

    public int getClickCount() {
        Waiter.waitForElementToAppear(spanClickCount);
        return Integer.parseInt(spanClickCount.getText());
    }
}
