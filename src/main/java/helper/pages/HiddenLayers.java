package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HiddenLayers extends CorePage {

    public HiddenLayers(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button#blueButton")
    private WebElement buttonBlueButton;

    @FindBy(css = "button#greenButton")
    private WebElement buttonGreenButton;

    public HiddenLayers clickGreenButton() {
        Waiter.waitForElementToAppear(buttonGreenButton);
        jsClick(buttonGreenButton);
        return this;
    }

    public void jsClick(WebElement element) {
        ((JavascriptExecutor) getDriver()).executeScript("return arguments[0].click();", element);
    }

    public boolean isGreenButtonDisplayed() {
        Waiter.waitForElementToAppear(buttonGreenButton);
        return buttonGreenButton.isDisplayed();
    }

    public boolean isBlueButtonDisplayed() {
        Waiter.waitForElementToAppear(buttonBlueButton);
        return buttonBlueButton.isDisplayed();
    }

}
