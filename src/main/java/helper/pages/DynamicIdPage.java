package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DynamicIdPage extends CorePage {

    public DynamicIdPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "h4 + button.btn.btn-primary")
    private WebElement buttonWithDynamicId;


    public DynamicIdPage clickButtonWithDynamicId() {
        Waiter.waitForElementToAppear(buttonWithDynamicId);
        buttonWithDynamicId.click();
        return this;
    }

    public String getTextButtonWithDynamicId() {
        return buttonWithDynamicId.getText();
    }

}
