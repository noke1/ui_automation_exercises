package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoadDelays extends CorePage{

    public LoadDelays(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button.btn.btn-primary")
    private WebElement buttonAppearingAfterDelay;

    public boolean isButtonAppearingAfterDelayDisplayed() {
        Waiter.waitForElementToAppear(buttonAppearingAfterDelay);
        return buttonAppearingAfterDelay.isDisplayed();
    }

    public LoadDelays clickButtonAppearingAfterDelay() {
        Waiter.waitForElementToAppear(buttonAppearingAfterDelay);
        buttonAppearingAfterDelay.click();
        return this;
    }

}
