package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NonBreakingSpace extends CorePage{
    public NonBreakingSpace(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//button[text()='My\u00A0Button']")
    private WebElement buttonMyButton;

    public boolean isMyButtonDisplayed() {
        Waiter.waitForElementToAppear(buttonMyButton);
        return buttonMyButton.isDisplayed();
    }


}
