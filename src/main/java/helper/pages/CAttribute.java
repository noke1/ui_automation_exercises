package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CAttribute extends CorePage {

    public CAttribute(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//button[contains(concat(' ', normalize-space(@class), ' '), ' btn-primary ')]")
    private WebElement buttonBlueButton;


    public CAttribute clickButtonBlueButton() {
        Waiter.waitForElementToAppear(buttonBlueButton);
        buttonBlueButton.click();
        return this;
    }

    public boolean isButtonBlueButtonDisplayed() {
        Waiter.waitForElementToAppear(buttonBlueButton);
        return buttonBlueButton.isDisplayed();
    }

    public String getAlertText() {
        try {
            Waiter.waitForAlertToAppear();
            Alert alert = getDriver().switchTo().alert();
            return alert.getText();
        } catch (NoAlertPresentException e) {
            return String.valueOf(e);
        }
    }

    public void acceptAlertBlueButtonClicked() {
        try {
            Waiter.waitForAlertToAppear();
            Alert alert = getDriver().switchTo().alert();
            alert.accept();
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }
}
