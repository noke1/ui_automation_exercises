package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DynamicTable extends CorePage {
    public DynamicTable(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "span[role=\"cell\"]")
    private List<WebElement> tableRows;

    @FindBy(css = "span[role=\"columnheader\"]")
    private List<WebElement> tableHeaders;

    @FindBy(css = "p.bg-warning")
    private WebElement labelChromeCpu;

    public String getLabelChromeCpuText() {
        Waiter.waitForElementToAppear(labelChromeCpu);
        return labelChromeCpu
                .getText()
                .replace("Chrome CPU:","")
                .trim();
    }

    public String getCpuValueForTask(String taskName) {
        return mapTableContentToTasks().stream()
                .filter(x -> x.getName().equals(taskName))
                .map(Task::getCpu)
                .findFirst()
                .orElse("Value Not Found");
    }

    /**
     * So columns inside the table are in random order (except column "Name").
     * To crete Task objects i need to know what is the order of the columns.
     * When I know the order, it is easy to create objects - just iterate and add offset from indexOf.
     * @return
     */
    private List<Task> mapTableContentToTasks() {
        List<Task> tasks = new ArrayList<>();
        List<String> headers = getTableHeaders();

        for (int i = 0; i < tableRows.size()-4; i+=5) {
            tasks.add(new Task(
                    tableRows.get(i + headers.indexOf("Name")).getText().trim(),
                    tableRows.get(i + headers.indexOf("Disk")).getText().trim(),
                    tableRows.get(i + headers.indexOf("Network")).getText().trim(),
                    tableRows.get(i + headers.indexOf("Memory")).getText().trim(),
                    tableRows.get(i + headers.indexOf("CPU")).getText().trim()
            ));
        }
        return tasks;
    }


    private List<String> getTableHeaders() {
        return tableHeaders.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

}

class Task{
    private String name;
    private String disk;
    private String network;
    private String memory;
    private String cpu;

    public Task(String name, String disk, String network, String memory, String cpu) {
        this.name = name;
        this.disk = disk;
        this.network = network;
        this.memory = memory;
        this.cpu = cpu;
    }

    public String getCpu() {
        return cpu;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", disk='" + disk + '\'' +
                ", network='" + network + '\'' +
                ", memory='" + memory + '\'' +
                ", cpu='" + cpu + '\'' +
                '}';
    }
}
