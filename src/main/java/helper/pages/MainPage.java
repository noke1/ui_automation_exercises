package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends CorePage{

    public MainPage(WebDriver driver) {
        super(driver);
    }



    @FindBy(css = "a[href=\"/dynamicid\"]")
    private WebElement anchorDynamicId;

    @FindBy(css = "a[href=\"/classattr\"]")
    private WebElement anchorClassAttribute;

    @FindBy(css = "h3 > a[href=\"/hiddenlayers\"]")
    private WebElement anchorHiddenLayers;

    @FindBy(css = "h3 > a[href=\"/loaddelay\"]")
    private WebElement anchorLoadDelays;

    @FindBy(css = "h3 > a[href=\"/ajax\"]")
    private WebElement anchorAjaxData;

    @FindBy(css = "h3 > a[href=\"/clientdelay\"]")
    private WebElement anchorClientSideDelay;

    @FindBy(css = "h3 > a[href=\"/click\"]")
    private WebElement anchorClickExercise;

    @FindBy(css = "h3 > a[href=\"/textinput\"]")
    private WebElement anchorTextInput;

    @FindBy(css = "h3 > a[href=\"/scrollbars\"]")
    private WebElement anchorScrollbars;

    @FindBy(css = "h3 > a[href=\"/dynamictable\"]")
    private WebElement anchorDynamicTable;

    @FindBy(css = "h3 > a[href=\"/verifytext\"]")
    private WebElement anchorVerifyText;

    @FindBy(css = "h3 > a[href=\"/progressbar\"]")
    private WebElement anchorProgressBar;

    @FindBy(css = "h3 > a[href=\"/visibility\"]")
    private WebElement anchorVisibility;

    @FindBy(css = "h3 > a[href=\"/sampleapp\"]")
    private WebElement anchorSampleApp;

    @FindBy(css = "h3 > a[href=\"/mouseover\"]")
    private WebElement anchorMouseOver;

    @FindBy(css = "h3 > a[href=\"/nbsp\"]")
    private WebElement anchorNonBreakingSpace;

    public DynamicIdPage goToDynamicIdPage() {
        Waiter.waitForElementToAppear(anchorDynamicId);
        anchorDynamicId.click();
        return new DynamicIdPage(getDriver());
    }

    public CAttribute goToClassAttributePage() {
        Waiter.waitForElementToAppear(anchorClassAttribute);
        anchorClassAttribute.click();
        return new CAttribute(getDriver());
    }

    public HiddenLayers goToHiddenLayersPage() {
        Waiter.waitForElementToAppear(anchorHiddenLayers);
        anchorHiddenLayers.click();
        return new HiddenLayers(getDriver());
    }

    public LoadDelays goToLoadDelaysPage() {
        Waiter.waitForElementToAppear(anchorLoadDelays);
        anchorLoadDelays.click();
        return new LoadDelays(getDriver());
    }

    public LoadDelays goToAjaxData() {
        Waiter.waitForElementToAppear(anchorAjaxData);
        anchorAjaxData.click();
        return new AjaxData(getDriver());
    }

    public ClickExercise goToClickExercise() {
        Waiter.waitForElementToAppear(anchorClickExercise);
        anchorClickExercise.click();
        return new ClickExercise(getDriver());
    }

    public ClientSideDelay goToClientSideDelay() {
        Waiter.waitForElementToAppear(anchorClientSideDelay);
        anchorClientSideDelay.click();
        return new ClientSideDelay(getDriver());
    }

    public TextInput goToTextInput() {
        Waiter.waitForElementToAppear(anchorTextInput);
        anchorTextInput.click();
        return new TextInput(getDriver());
    }

    public ScrollBars goToScrollbars() {
        Waiter.waitForElementToAppear(anchorScrollbars);
        anchorScrollbars.click();
        return new ScrollBars(getDriver());
    }

    public DynamicTable goToDynamicTable() {
        Waiter.waitForElementToAppear(anchorDynamicTable);
        anchorDynamicTable.click();
        return new DynamicTable(getDriver());
    }

    public VerifyText goToVerifyText() {
        Waiter.waitForElementToAppear(anchorVerifyText);
        anchorVerifyText.click();
        return new VerifyText(getDriver());
    }

    public ProgressBar goToProgressBar() {
        Waiter.waitForElementToAppear(anchorProgressBar);
        anchorProgressBar.click();
        return new ProgressBar(getDriver());
    }

    public Visibility goToVisibility() {
        Waiter.waitForElementToAppear(anchorVisibility);
        anchorVisibility.click();
        return new Visibility(getDriver());
    }

    public SampleApp goToSampleApp() {
        Waiter.waitForElementToAppear(anchorSampleApp);
        anchorSampleApp.click();
        return new SampleApp(getDriver());
    }

    public MouseOver goToMouseOver() {
        Waiter.waitForElementToAppear(anchorMouseOver);
        anchorMouseOver.click();
        return new MouseOver(getDriver());
    }

    public MouseOver goToNonBreakingSpace() {
        Waiter.waitForElementToAppear(anchorNonBreakingSpace);
        anchorNonBreakingSpace.click();
        return new MouseOver(getDriver());
    }





}
