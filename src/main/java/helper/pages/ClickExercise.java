package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;

public class ClickExercise extends CorePage {

    public ClickExercise(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button#badButton.btn.btn-primary")
    private WebElement buttonTriggerEventBlue;

    @FindBy(css = "button.btn.btn-success")
    private WebElement buttonTriggerEventGreen;

    public WebElement getButtonTriggerEventBlue() {
        return buttonTriggerEventBlue;
    }

    public WebElement getButtonTriggerEventGreen() {
        return buttonTriggerEventGreen;
    }

    public ClickExercise clickButtonTriggerEvent() {
        Waiter.waitForElementToAppear(buttonTriggerEventBlue);
        Actions actions = new Actions(getDriver());
        actions.moveToElement(buttonTriggerEventBlue).build().perform();
        buttonTriggerEventBlue.click();
        return this;
    }

    public String getClassesOfElement(WebElement element) {
        Waiter.waitForElementToAppear(element);
//        String buttonColor = element.getCssValue("background-color");
        return element.getAttribute("class");
    }


}
