package helper.pages;

import helper.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Visibility extends CorePage{

    public Visibility(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "hideButton")
    private WebElement buttonHide;

    @FindBy(id = "removedButton")
    private WebElement buttonRemoved;

    @FindBy(id = "zeroWidthButton")
    private WebElement buttonZeroWidth;

    @FindBy(id = "overlappedButton")
    private WebElement buttonOverlapped;

    @FindBy(id = "transparentButton")
    private WebElement buttonOpacityZero;

    @FindBy(id = "invisibleButton")
    private WebElement buttonVisibilityHidden;

    @FindBy(id = "notdisplayedButton")
    private WebElement buttonDisplayNone;

    @FindBy(id = "offscreenButton")
    private WebElement buttonOffscreen;


    public Visibility clickButtonHide() {
        Waiter.waitForElementToAppear(buttonHide);
        buttonHide.click();
        return this;
    }

    public boolean isElementNotVisible(WebElement element) {
        return Waiter.isElementNotVisible(element);
    }

    public WebElement getButtonRemoved() {
        return buttonRemoved;
    }

    public WebElement getButtonZeroWidth() {
        return buttonZeroWidth;
    }

    public WebElement getButtonOverlapped() {
        return buttonOverlapped;
    }

    public WebElement getButtonOpacityZero() {
        return buttonOpacityZero;
    }

    public WebElement getButtonVisibilityHidden() {
        return buttonVisibilityHidden;
    }

    public WebElement getButtonDisplayNone() {
        return buttonDisplayNone;
    }

    public WebElement getButtonOffscreen() {
        return buttonOffscreen;
    }
}
