import helper.pages.MainPage;
import helper.pages.NonBreakingSpace;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NonBreakingSpaceTest extends CoreTest {

    private MainPage mainPage;
    private NonBreakingSpace nonBreakingSpace;

    @Test
    public void checkIfButtonWithNBSPIsDisplayed() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        nonBreakingSpace = new NonBreakingSpace(getDriver());

        mainPage.goToNonBreakingSpace();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        final boolean isMyButtonDisplayed = nonBreakingSpace.isMyButtonDisplayed();

        assertThat(isMyButtonDisplayed)
                .isTrue();
    }
}
