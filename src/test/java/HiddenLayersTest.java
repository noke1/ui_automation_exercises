import helper.pages.HiddenLayers;
import helper.pages.MainPage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class HiddenLayersTest extends CoreTest {

    private MainPage mainPage;
    private HiddenLayers hiddenLayers;

    @Test
    public void shouldCheckIfGreenButtonIsDisplayed() {
        mainPage = new MainPage(getDriver());
        hiddenLayers = new HiddenLayers(getDriver());

        mainPage.goToHiddenLayersPage();

        assertThat(hiddenLayers.isGreenButtonDisplayed())
                .isTrue();
    }

    @Test
    public void shouldCheckIfButtonChangesFromGreenToBlueAfterClick() {
        mainPage = new MainPage(getDriver());
        hiddenLayers = new HiddenLayers(getDriver());

        mainPage.goToHiddenLayersPage();
        hiddenLayers.clickGreenButton();

        assertThat(hiddenLayers.isBlueButtonDisplayed())
                .isTrue();
    }

}
