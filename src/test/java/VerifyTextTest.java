import helper.pages.MainPage;
import helper.pages.VerifyText;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class VerifyTextTest extends CoreTest {

    private MainPage mainPage;
    private VerifyText verifyText;

    @Test
    public void welcomeTextShouldBeEqualToWelcome() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        verifyText = new VerifyText(getDriver());

        mainPage.goToVerifyText();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        final String WELCOME_TEXT = verifyText.getWelcomeText();
        final String ACTUAL_WELCOME_TEXT = "Welcome UserName!";

        Assertions.assertThat(WELCOME_TEXT)
                .isEqualTo(ACTUAL_WELCOME_TEXT);
    }

}
