import helper.pages.MainPage;
import helper.pages.ProgressBar;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class ProgressBarTest extends CoreTest{

    private MainPage mainPage;
    private ProgressBar progressBar;


    @Test
    public void shouldCheckIfResultIs0AfterStopOn75Percent() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        progressBar = new ProgressBar(getDriver());

        mainPage.goToProgressBar();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        progressBar
                .clickButtonStart()
                .stopProgressIfValueOf("75%");
        final int actualResult = progressBar.getResultValue();

        assertThat(actualResult).isBetween(-2,2);
    }

}
