import helper.pages.MainPage;
import helper.pages.Visibility;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Those tests are duplicated because of as() method in each of them.
 * as() method tells why each button is not visible. And this is just a project to learn webdriver behavior.
 */
public class VisibilityTest extends CoreTest {

    private MainPage mainPage;
    private Visibility visibility;

    @Test
    public void shouldCheckIfRemovedButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonRemoved());

        assertThat(isNotVisible)
                .as("Removed button is no longer present cause it was deleted")
                .isTrue();
    }
    
    @Test
    public void shouldCheckIfZeroWidthButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonZeroWidth());

        assertThat(isNotVisible)
                .as("Zero Width button is no longer present cause of width 0")
                .isTrue();
    }

    @Test
    public void shouldCheckIfOverlappedButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonOverlapped());

        assertThat(isNotVisible)
                .as("Overlapped button is no longer visible cause of overlapped")
                .isTrue();
    }
    @Test
    public void shouldCheckIfOpacityZeroButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonOpacityZero());

        assertThat(isNotVisible)
                .as("OpacityZero button is no longer visible cause of opacity:0")
                .isTrue();
    }
    @Test
    public void shouldCheckIfVisibilityHiddenButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonVisibilityHidden());

        assertThat(isNotVisible)
                .as("VisibilityHidden button is no longer visible cause of visibility:hidden")
                .isTrue();
    }
    @Test
    public void shouldCheckIfDisplayNoneButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonDisplayNone());

        assertThat(isNotVisible)
                .as("DisplayNone button is no longer visible cause of display:none")
                .isTrue();
    }
    @Test
    public void shouldCheckIfOffscreenButtonIsNoLongerVisible() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        visibility = new Visibility(getDriver());

        mainPage.goToVisibility();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        visibility.clickButtonHide();
        final boolean isNotVisible = visibility.isElementNotVisible(visibility.getButtonOffscreen());

        assertThat(isNotVisible)
                .as("Offscreen is no longer visible cause offscreen")
                .isTrue();
    }

}
