import helper.pages.ClientSideDelay;
import helper.pages.MainPage;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ClientSideDelayTest extends CoreTest {

    private MainPage mainPage;
    private ClientSideDelay clientSideDelay;

    @Test
    public void shouldCheckIfTextOfLabelMatches() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        clientSideDelay = new ClientSideDelay(getDriver());

        mainPage.goToClientSideDelay();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        clientSideDelay.clickButtonTriggerLogic();
        final String expectedLabelText = "Data calculated on the client side.";
        final String actualLabelText = clientSideDelay.getLabelClientSideDelayText();

        Assertions.assertThat(actualLabelText)
                .as("Check if text from label after delay matches")
                .isEqualTo(expectedLabelText);
    }

}
