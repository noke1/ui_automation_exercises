import helper.browser.Browser;
import helper.browser.BrowserAdjust;
import helper.browser.BrowserTerminate;
import helper.browser.ChromeInitialize;
import helper.waiter.Waiter;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

public class CoreTest {

    private Browser browser;
    private static WebDriver driver;

    public CoreTest() {
        browser = new Browser(
                new ChromeInitialize(),
                new BrowserTerminate(),
                new BrowserAdjust(),
                "CHROME"
        );
    }

    public static WebDriver getDriver() {
        return driver;
    }

    @Before
    public void before() {
        driver = browser.initialize();
        browser.adjust(getDriver());
    }


    @After
    public void after() {
        browser.terminate(getDriver());
    }
}
