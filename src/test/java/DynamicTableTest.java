import helper.pages.DynamicTable;
import helper.pages.MainPage;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class DynamicTableTest extends CoreTest {

    private MainPage mainPage;
    private DynamicTable dynamicTable;

    @Test
    public void test123() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        dynamicTable = new DynamicTable(getDriver());

        mainPage.goToDynamicTable();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        final String actualChromeCpuValue = dynamicTable.getCpuValueForTask("Chrome");
        final String expectedChromeCpuValue = dynamicTable.getLabelChromeCpuText();

        assertThat(actualChromeCpuValue)
                .isEqualToNormalizingWhitespace(expectedChromeCpuValue);
    }


}
