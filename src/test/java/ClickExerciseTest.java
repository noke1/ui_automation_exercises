import helper.pages.ClickExercise;
import helper.pages.MainPage;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ClickExerciseTest extends CoreTest {

    private MainPage mainPage;
    private ClickExercise clickExercise;

    @Test
    public void shouldCheckIfButtonIsGreenAfterClick() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        clickExercise = new ClickExercise(getDriver());

        mainPage.goToClickExercise();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        clickExercise.clickButtonTriggerEvent();
        final String greenButtonBackgroundClasses = clickExercise.getClassesOfElement(clickExercise.getButtonTriggerEventGreen());

        Assertions.assertThat(greenButtonBackgroundClasses)
                .as("Check if button classes changed. And if so color must be changed to green")
                .isEqualTo("btn btn-success");
    }

}
