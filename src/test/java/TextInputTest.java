import helper.pages.MainPage;
import helper.pages.TextInput;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TextInputTest extends CoreTest {

    private MainPage mainPage;
    private TextInput textInput;

    @Test
    public void shouldCheckIfButtonNameChanges() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        textInput = new TextInput(getDriver());
        final String nameToChange = "TEST";

        mainPage.goToTextInput();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        textInput
                .insertTextOf(nameToChange)
                .clickButtonCreateNewButton();
        final String text = textInput.getCreateNewButtonText();

        assertThat(text)
                .isEqualTo(nameToChange);
    }


}
