import helper.pages.LoadDelays;
import helper.pages.MainPage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LoadDelaysTest extends CoreTest {

    private MainPage mainPage;
    private LoadDelays loadDelays;

    @Test
    public void shouldCheckIfButtonAppearingAfterDelayIsDisplayed() {
        mainPage = new MainPage(getDriver());
        loadDelays = new LoadDelays(getDriver());

        mainPage.goToLoadDelaysPage();
        loadDelays.clickButtonAppearingAfterDelay();

        assertThat(loadDelays.isButtonAppearingAfterDelayDisplayed())
                .isFalse();
    }
}
