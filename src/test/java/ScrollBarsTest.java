import helper.pages.MainPage;
import helper.pages.ScrollBars;
import org.junit.Test;

public class ScrollBarsTest extends CoreTest {

    private MainPage mainPage;
    private ScrollBars scrollBars;

    @Test
    public void shouldClickHiddenButton() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        scrollBars = new ScrollBars(getDriver());

        mainPage.goToScrollbars();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        scrollBars.clickButtonHiding();
        //nothing happens after clicking so no assertion is made
    }

}
