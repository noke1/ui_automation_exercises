import helper.pages.CAttribute;
import helper.pages.MainPage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CAttributeTest extends CoreTest {

    private MainPage mainPage;
    private CAttribute cAttribute;

    @Test
    public void shouldCheckIfBlueButtonIsDisplayed() {
        mainPage = new MainPage(getDriver());
        cAttribute = new CAttribute(getDriver());

        mainPage.goToClassAttributePage();

        assertThat(cAttribute.isButtonBlueButtonDisplayed())
                .isTrue();
    }

    @Test
    public void shouldCheckIfAlertTextIsCorrect() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        cAttribute = new CAttribute(getDriver());

        mainPage.goToClassAttributePage();
        cAttribute.clickButtonBlueButton();
        String actualAlertText = cAttribute.getAlertText();

        assertThat(actualAlertText)
                .as("Check if alert is visible after clicking blue button")
                .isEqualTo("Primary button pressed");
    }
}
