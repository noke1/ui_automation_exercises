import helper.pages.AjaxData;
import helper.pages.MainPage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AjaxDataTest extends CoreTest {

    private MainPage mainPage;
    private AjaxData ajaxData;

    @Test
    public void shouldCheckIfLabelTextMatchesExpected() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        ajaxData = new AjaxData(getDriver());

        mainPage.goToAjaxData();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        ajaxData.clickButtonTrigger();
        final String expectedLabelText = "Data loaded with AJAX get request.";
        final String actualLabelText = ajaxData.getLabelDataLoadedText();

        assertThat(actualLabelText)
                .as("Check if text from label after delay matches")
                .isEqualTo(expectedLabelText);
    }
}
