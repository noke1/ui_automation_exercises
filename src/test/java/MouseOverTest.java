import helper.pages.MainPage;
import helper.pages.MouseOver;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MouseOverTest extends CoreTest {
    private MainPage mainPage;
    private MouseOver mouseOver;

    @Test
    public void checkIfDoubleClickIncreasesCounterTo2() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        mouseOver = new MouseOver(getDriver());

        mainPage.goToMouseOver();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        mouseOver
                .hoverAndClickClickMe()
                .hoverAndClickClickMe();
        final int clickCount = mouseOver.getClickCount();

        assertThat(clickCount).isEqualTo(2);
    }
}
