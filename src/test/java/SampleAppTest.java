import helper.pages.MainPage;
import helper.pages.SampleApp;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class SampleAppTest extends CoreTest {

    private MainPage mainPage;
    private SampleApp sampleApp;


    @Test
    public void successMessageAppearsAfterSuccessfulLogin() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        sampleApp = new SampleApp(getDriver());
        final String username = "slawomir_ruracki";
        final String password = "pwd";

        mainPage.goToSampleApp();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        sampleApp.loginWith(username, password);
        final String welcomeMessage = sampleApp.getMessageAfterLogIn();

        assertThat(String.format("Welcome, %s!", username))
                .as("After passing correct credentials welcome message appears")
                .isEqualTo(welcomeMessage);
    }

    @Test
    public void failedMessageAppearsAfterLoginWithWrongPassword() throws InterruptedException {
        mainPage = new MainPage(getDriver());
        sampleApp = new SampleApp(getDriver());
        final String username = "slawomir_ruracki";
        final String wrongPassword = "pwd1";

        mainPage.goToSampleApp();
        Thread.sleep(1000); // i dont have a plan for this just now so it stays  like this :(
        sampleApp.loginWith(username, wrongPassword);
        final String failLogInMessage = sampleApp.getMessageAfterLogIn();

        assertThat("Invalid username/password") // "/" is not a special character and no need to escape it
                .as("After passing wrong password fail message appears")
                .isEqualTo(failLogInMessage);
    }

}
