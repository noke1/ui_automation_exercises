import helper.pages.DynamicIdPage;
import helper.pages.MainPage;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class DynamicIdTest extends CoreTest {

    private MainPage mainPage;
    private DynamicIdPage dynamicIdPage;

    @Test
    public void shouldClickButtonWithDynamicId() {
        mainPage = new MainPage(getDriver());
        dynamicIdPage = new DynamicIdPage(getDriver());

        mainPage.goToDynamicIdPage();
        dynamicIdPage.clickButtonWithDynamicId();
    }

    @Test
    public void shouldCheckIfTextInButtonMatches() {
        mainPage = new MainPage(getDriver());
        dynamicIdPage = new DynamicIdPage(getDriver());

        mainPage.goToDynamicIdPage();
        final String actualButtonText = dynamicIdPage.getTextButtonWithDynamicId();

        Assertions.assertThat(actualButtonText)
                .as("Button text should match expected test")
                .isEqualTo("Button with Dynamic ID");
    }
}
