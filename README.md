Practice tasks from [uiautomationplayground](https://www.uitestingplayground.com/)

### Quick description:
This is a project just to train some Selenium interactions. Don't look for any fancy solutions, objectivity, reporting, parallel execution and all that clean code stuff.
Main challenge is to solve exercises and whole framework-based problems are on the second plan.

Each exercise has its own page object inside pages folder.

Each exercise solution is placed inside test class in test folder.

On a daily basis I used Java+Selenium+Maven+TestNG so this project is build with JAVA+Selenium+JUnit+Gradle.

PS. I went a little lazy with those sleeps inside test methods :)
